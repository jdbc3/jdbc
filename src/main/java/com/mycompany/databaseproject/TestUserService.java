/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.Service.UserService;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author BankMMT
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("peem","password");
        if (user!=null){
            System.out.println("Welcome User : "+user.getName());
        }else {
            System.out.println("Error");
        }
    }
}
